Feature:
  In order to have my todos protected
  As a user
  I want it to be necessary to login to view todos


  Scenario: Redirection to login form
    Given I am not logged in
    When I access "todos list" page
    Then I should be redirected to "login" page