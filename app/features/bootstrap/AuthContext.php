<?php declare(strict_types=1);

use Behat\MinkExtension\Context\RawMinkContext;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class AuthContext extends RawMinkContext
{
    /** @var Session */
    private $session;

    /** @var UserManagerInterface */
    private $userManager;

    /**
     * @param Session $session
     * @param UserManagerInterface $userManager
     */
    public function __construct(Session $session, UserManagerInterface $userManager)
    {
        $this->session = $session;
        $this->userManager = $userManager;
    }

    /**
     * @Given I am logged as :username with :password
     */
    public function iAmLoggedAs(string $username, string $password): void
    {
        $this->visitPath($this->getMinkParameter('base_url').'/login');

        $page = $this->getSession()->getPage();
        $page->fillField('Username', $username);
        $page->fillField('Password', $password);
        $page->pressButton('Log in');
    }

    /**
     * @Given I am not logged in
     */
    public function iAmNotLoggedIn(): void
    {
        $driver = $this->getSession()->getDriver();
        /** @var Goutte\Client $client */
        $client = $driver->getClient();
        $client->getCookieJar()->clear();
    }
}
