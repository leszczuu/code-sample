<?php

use App\Entity\Todo;
use App\Entity\User;
use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\TableNode;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;


class FeatureContext implements Context
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var Response|null
     */
    private $response;

    /** @var ObjectManager */
    private $entityManager;

    public function __construct(KernelInterface $kernel, ObjectManager $entityManager)
    {
        $this->kernel = $kernel;
        $this->entityManager = $entityManager;
    }

    /**
     * @Then the response should be received
     */
    public function theResponseShouldBeReceived()
    {
        if ($this->response === null) {
            throw new \RuntimeException('No response received');
        }
    }

    /**
     * @Given following todos exists
     */
    public function followingTodosExists(TableNode $table)
    {
        $todoRepository = $this->entityManager->getRepository(Todo::class);
        $userRepository = $this->entityManager->getRepository(User::class);

        $preexistingTodos = $todoRepository->findAll();
        foreach ($preexistingTodos as $todo) {
            $this->entityManager->remove($todo);
        }

        foreach($table->getColumnsHash() as $todoData) {
            $newTodo = new Todo($todoData['description'], $todoData['doneness'] === 'true');
            $user = $userRepository->findOneBy(['username' => $todoData['owner']]);
            $newTodo->setOwner($user);

            $this->entityManager->persist($newTodo);
        }

        $this->entityManager->flush();
    }

}
