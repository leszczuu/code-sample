<?php declare(strict_types=1);

use Behat\Gherkin\Node\TableNode;
use Behat\Mink\Element\NodeElement;
use Behat\MinkExtension\Context\MinkContext as BehatMinkContext;


class MinkContext extends BehatMinkContext
{

    private $pages = [
        'todos list' => '/todos',
        'login' => '/login',
        'new todo' => '/todos/new',
    ];

    /**
     * @When I access :pageName page
     */
    public function iAccessPage(string $pageName)
    {
        $this->getSession()->visit($this->toUrl($pageName));
    }

    /**
     * @Then I should be redirected to :pageName page
     */
    public function iShouldBeRedirectedToPage($pageName)
    {
        $url = $this->getSession()->getCurrentUrl();
        $expected = $this->toUrl($pageName);

        if ($url !== $expected) {
            throw new \RuntimeException("Expected ${expected} got ${url}.");
        }
    }

    /**
     * @Then the response should contain only following todos:
     */
    public function theResponseShouldContainOnlyFollowingTodos(TableNode $expected)
    {
        $pageTodosEncoded = array_map('json_encode', $this->extractTodosFromPage());
        $expectedTodosEncoded = $this->mapExpectedToJsonEncoded($expected);
        $notExpectedTodos = array_diff($pageTodosEncoded, $expectedTodosEncoded);
        $notFoundTodos = array_diff($expectedTodosEncoded, $pageTodosEncoded);

        $this->assertNoUnexpected($notExpectedTodos);
        $this->assertNoNotFound($notFoundTodos);
    }

    /**
     * @Then the response should contain following todos:
     */
    public function theResponseShouldContainFollowingTodos(TableNode $expected)
    {
        $pageTodosEncoded = array_map('json_encode', $this->extractTodosFromPage());
        $expectedTodosEncoded = $this->mapExpectedToJsonEncoded($expected);
        $notFoundTodos = array_diff($expectedTodosEncoded, $pageTodosEncoded);

        $this->assertNoNotFound($notFoundTodos);
    }

    /**
     * @param string $pageName
     * @return string
     */
    private function toUrl(string $pageName): string
    {
        if (!isset($this->pages[$pageName])) {
            throw new \RuntimeException("Couldn't create url for ${pageName} page.");
        }

        return $this->getMinkParameter('base_url').$this->pages[$pageName];
    }

    /**
     * @return array
     */
    private function extractTodosFromPage(): array
    {
        $page = $this->getSession()->getPage();
        $pageTodos = array_map(
            function (NodeElement $todo) {
                $description = $todo->find('css', 'p')->getText();
                $doneness = $todo->find('css', 'p')->getAttribute('class') === 'done';

                return compact('description', 'doneness');
            },
            $page->findAll('css', 'ul.todos li')
        );

        return $pageTodos;
    }

    /**
     * @param TableNode $expected
     * @return array
     */
    private function mapExpectedToJsonEncoded(TableNode $expected): array
    {
        $expectedTodos = array_map(
            function (array $hash) {
                return [
                    'description' => $hash['description'],
                    'doneness' => $hash['doneness'] === 'true',
                ];
            },
            $expected->getColumnsHash()
        );
        $expectedTodosEncoded = array_map('json_encode', $expectedTodos);

        return $expectedTodosEncoded;
    }

    /**
     * @param $notExpectedTodos
     */
    private function assertNoUnexpected($notExpectedTodos): void
    {
        if ($notExpectedTodos) {
            $implodedNotExpected = implode(', ', $notExpectedTodos);
            throw new RuntimeException("Found not expected todos: ${implodedNotExpected}.");
        }
    }

    /**
     * @param $notFoundTodos
     */
    private function assertNoNotFound($notFoundTodos): void
    {
        if ($notFoundTodos) {
            $implodedNotFound = implode(', ', $notFoundTodos);
            throw new RuntimeException("Found not expected todos: ${implodedNotFound}.");
        }
    }
}
