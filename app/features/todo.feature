Feature:
  As a user
  I want to be able to see todos list

  Scenario: Seeing list of my todos
    Given I am logged as "user" with "password"
    And following todos exists
    | owner  | description       | doneness |
    | user   | some description  | false    |
    | user   | other description | false    |
    When I access "todos list" page
    Then the response should contain only following todos:
      | description       | doneness |
      | some description  | false    |
      | other description | false    |

  Scenario: Adding new todo
    Given I am logged as "user" with "password"
    When I access "new todo" page
    And I fill in the following:
      | form_description | my new todo description |
    And I press "Create Todo"
    Then I should be redirected to "todos list" page
    And the response should contain following todos:
      | description  | doneness |
