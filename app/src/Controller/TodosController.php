<?php

namespace App\Controller;

use App\Entity\Todo;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @IsGranted("ROLE_USER")
 **/
class TodosController extends Controller
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @Route("/todos", name="todos")
     */
    public function index()
    {
        $user = $this->getLoggedInUser();
        $todos = $user->getTodos();

        return $this->render(
            'todos/all_todos.html.twig',
            [
                'todos' => $todos,
            ]
        );
    }

    /**
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/todos/new", name="new_todo")
     */
    public function new(Request $request, ObjectManager $manager)
    {
        $todo = new Todo('', false);

        $form = $this->createFormBuilder($todo)
            ->add('description', TextareaType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Todo'))
            ->getForm();

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $todo->setOwner($this->getLoggedInUser());
            $manager->persist($todo);
            $manager->flush();

            return $this->redirectToRoute('todos');
        }

        return $this->render(
            'todos/new.html.twig',
            array(
            'form' => $form->createView(),
            )
        );
    }

    /**
     * @return User
     */
    private function getLoggedInUser(): User
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        return $user;
    }
}
