<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Todo", mappedBy="owner")
     */
    private $todos;

    /**
     */
    public function __construct()
    {
        $this->todos = new ArrayCollection();

        parent::__construct();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Sets the locking status of the user.
     *
     * @param boolean $boolean
     *
     * @return self
     */
    public function setLocked($boolean): self
    {
        //do nothing, We don't support locking out users at the moment
    }

    /**
     * @param Todo $todo
     */
    public function addTodo(Todo $todo): void
    {
        $this->todos->add($todo);
    }

    /**
     * @return Todo[]
     */
    public function getTodos(): Collection
    {
        return $this->todos;
    }
}
