<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TodoRepository")
 */
class Todo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $done;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="todos")
     */
    private $owner;

    /**
     * @param string $description
     * @param bool $done
     */
    public function __construct(string $description, bool $done)
    {
        $this->description = $description;
        $this->done = $done;
    }

    /**
     * @return int
     */
    public function getId():int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Todo
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getDone(): ?bool
    {
        return $this->done;
    }

    /**
     * @param bool $done
     * @return Todo
     */
    public function setDone(bool $done): self
    {
        $this->done = $done;

        return $this;
    }

    /**
     * @param User $user
     * @return Todo
     */
    public function setOwner(User $user): self
    {
        $this->owner = $user;

        return $this;
    }
}
