<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class InitialUserData extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $firstUser = new User();
        $firstUser->setUsername('user')
            ->setEmail('user@example.com')
            ->setPlainPassword('password')
            ->setEnabled(true)
            ->setRoles(['ROLE_USER']);

        $manager->persist($firstUser);
        $manager->flush();
    }
}
