# code sample

### Layout
- docker-compose.yml / docker directory contains docker related stuff
- app directory contains app code

### Running
- docker-compose up starts app

- docker-compose run --rm php-cli composer install -- installs composer dependencies
- docker-compose run --rm php-cli bin/console doctrine:migrations:migrate -- recreates DB schema from migrations
- docker-compose run --rm php-cli bin/console doctrine:fixtures:load -- Addes user 'user' with password 'password' to DB